import {danger, warn} from "danger"

const hasChangelog = danger.git.modified_files.includes("changelog.md")
const isTrivial = (danger.gitlab.mr.description + danger.gitlab.mr.title).includes("#trivial")

// validação changelog
if (!hasChangelog && !isTrivial) {
  warn("Por favor, adicione uma entrada de registro de alterações para suas alterações no changelog.md.")
}

// validação lock files
const packageChanged = danger.git.modified_files.includes('package.json');
const lockfileChanged = danger.git.modified_files.includes('yarn.lock');
if (packageChanged && !lockfileChanged) {
  const message = 'As alterações foram feitas no package.json, mas não no yarn.lock';
  const idea = 'Talvez você precise executar `yarn install`?';
  warn(`${message} - <i>${idea}</i>`);
}